package com.example.userservice.controllers;

import com.example.userservice.dto.ResponseUser;
import com.example.userservice.dto.TransactionDTO;
import com.example.userservice.exceptions.UserAlreadyExistsException;
import com.example.userservice.models.User;
import com.example.userservice.services.UserService;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    private final RestTemplate restTemplate = new RestTemplate();
    private final String API_TRANSACTION = "http://transaction-service:8086/api/v1/transactions/user";


    @GetMapping
    public List<User> getUsers() {
        return userService.getUsers();
    }

    @PostMapping
    public User createUser(@RequestBody User user) {
        return userService.createUser(user);
    }

    @GetMapping("/info")
    public User getUserById(@RequestParam("userId") Long id) {
        return userService.getUserById(id);
    }


    @PostMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") Long id) {
        if (userService.deleteUser(id)) {
            return "Delete User Success";
        }
        return "Delete User Fail!";
    }

    @PostMapping("/update")
    public String updateUser(@RequestBody User user) {
        if (userService.updateUser(user)) {
            return "Update User Success";
        }
        return "Update User Fail!";
    }

    @GetMapping("/transactions")
    @Retry(name = "transactionService")
    public ResponseUser getTransactionsForUser(@RequestParam("userId") Long id) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(API_TRANSACTION)
                .queryParam("userId", id);

        TransactionDTO[] transactionDTOS = restTemplate.getForObject(builder.toUriString(), TransactionDTO[].class);
        if (transactionDTOS == null) {
            throw new UserAlreadyExistsException("Not found transactions for user");
        }
        final var userById = userService.getUserById(id);
        return ResponseUser.builder()
                .phone(userById.getPhone())
                .name(userById.getName())
                .address(userById.getAddress())
                .cid(userById.getCid())
                .transactionDTOS(Arrays.asList(transactionDTOS))
                .build();


    }


}

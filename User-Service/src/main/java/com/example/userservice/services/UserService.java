package com.example.userservice.services;

import com.example.userservice.exceptions.UserAlreadyExistsException;
import com.example.userservice.models.User;
import com.example.userservice.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;

    public User createUser(User user) {
        final var userByPhone = userRepository.getUserByPhone(user.getPhone());
        final var userByEmail = userRepository.getUserByEmail(user.getEmail());
        final var userByCID = userRepository.getUserByCid(user.getCid());
        if (userByPhone.isPresent()) {
            throw new UserAlreadyExistsException("Phone number is exist!");
        }
        if (userByEmail.isPresent()) {
            throw new UserAlreadyExistsException("Email is exist");
        }
        if (userByCID.isPresent()) {
            throw new UserAlreadyExistsException("cid is exits");
        }
        return userRepository.save(user);
    }

    public boolean deleteUser(Long id) {
        if (userRepository.findById(id).isPresent()) {
            userRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public boolean updateUser(User user) {
        if (userRepository.findById(user.getId()).isPresent()) {
            userRepository.save(user);
            return true;

        }
        return false;
    }

    public User getUserById(Long id) {
        return userRepository.findById(id).orElseThrow(() -> new UserAlreadyExistsException("Not found user"));
    }

    public List<User> getUsers() {
        return userRepository.findAll();
    }

}

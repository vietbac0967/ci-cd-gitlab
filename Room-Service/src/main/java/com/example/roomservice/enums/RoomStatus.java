package com.example.roomservice.enums;

public enum RoomStatus {
    Available,   // Phòng trống, sẵn sàng cho khách thuê
    Occupied,    // Phòng đã có khách
    Reserved,    // Phòng đã được đặt trước
    UnderMaintenance, // Phòng đang được bảo trì
    OutOfService // Phòng không sử dụng được
}
